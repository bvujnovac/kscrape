import datetime
import time
import requests
import smtplib
from bs4 import BeautifulSoup
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from requests.exceptions import Timeout

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
}

login_url = 'https://www.konzum.hr/web/sign_in'
url = 'https://www.konzum.hr/web/raspolozivi-termini'
interval = 96 # interval for the while loop
email = "konzum_email@example.com" #konzum klik email address
password = "somepassword" # konzum klik password
owner = 'Myname Mysurname' # your registered konzum klik first and last name
gmail_user = 'email@example.com' # sending email account
gmail_password = 'somepassword' # sending email account password

timestamp = datetime.datetime.now().time() # get the date information
start = datetime.time(7, 30) # time interval from
end = datetime.time(21)# time interval to

warnning = '<h2>Greška dohvačanja podataka, moguća gužva na stranici!</h2>'

s = requests.Session()
# function to send emails
def notifyUser (SUBJECT, TEXT):
    li = ["email1@example.com", "email2@example.com"] #list of email id's to send the mail to.
    subject = SUBJECT
    text = TEXT

    # Create message container - the correct MIME type is multipart/alternative.
    message = MIMEMultipart('alternative')
    message['Subject'] = subject
    part2 = MIMEText(text, 'html')
    message.attach(part2)

    for i in range(len(li)):
        s = smtplib.SMTP('smtp.gmail.com', 587)
        s.starttls()
        s.login(gmail_user, gmail_password)
        s.sendmail(gmail_user, li[i], message.as_string())
        s.quit()

# function that checks available deliveries
def checkDelivery():
    try:
        page = s.get(url, headers = headers)
    except Timeout:
        print("Timeout occured")
    soup = BeautifulSoup(page.text, 'html.parser')

    # Pull all text from the section-availability-calendar section
    try:
        availibilitySection = soup.find(class_='section-availability-calendar')
    except:
        pass
    # Get availibility per type
    try:
        delivery = availibilitySection.find('div', {'data-tab-type':'delivery'})
        notOkay = False
    except:
        notOkay = True

    # Grab the status of each availibility type
    try:
        deliveryStatus = str(delivery.find('h2'))
    except:
        pass
    if notOkay is not True:
        if not 'Trenutno nema dostupnih termina' in deliveryStatus:
            print("Termin dostave poslan na email")
            notifyUser("Konzum Klik dostava", str(delivery))
            #if start <= timestamp <= end: # can be used to send notification in desired time interval (just place the code inside if function)
    else:
        print("Poruka greške poslana")
        notifyUser("Konzum Klik greška", str(warnning))

# function that creates a session and logs in into the site
def siteLogin ():
    login_site = s.get(login_url, headers = headers)
    login_content = BeautifulSoup(login_site.content, "html.parser")
    token = login_content.find("meta", {"name":"csrf-token"})["content"]
    login_data = {"utf8":"✓","authenticity_token":token,"spree_user[email]":email,"spree_user[password]":password,"spree_user[remember_me]":"0","button":""}
    s.post(login_url,login_data)

#### main section ###
try:
    while True:
        checkLogin = s.get(url, headers = headers)
        content = BeautifulSoup(checkLogin.text, "html.parser")
        myName = str(content.find('strong'))
        time.sleep(2)
        if not owner in myName:
            print("logging in first")
            siteLogin()
            time.sleep(2)
            checkDelivery()
        else:
            print("just checking delivery")
            checkDelivery()
        time.sleep(interval)
except KeyboardInterrupt:
    pass
