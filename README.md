**Kscrape** (Konzum klik scrape)

small python script written to scrape https://www.konzum.hr/web/raspolozivi-termini for available delivery status.

This script runs in a while loop to periodically check the site for available delivery options. Can be used with nodejs forever to ensure uptime.

If the h2 field of each delivery section doesn't contain 'Trenutno nema dostupnih termina' it will email the entire section (containing available delivery times) HTML part to the email you entered.


Make sure to change the following lines in Kscrape.py to use your data.
Kscrape.py creates a login session on https://www.konzum.hr/web/sign_in and checkes the delivery status based on your konzum klik delivery address. 

```
...
interval = 96 # interval for the while loop
email = "konzum_email@example.com" #konzum klik email address
password = "somepassword" # konzum klik password
owner = 'Myname Mysurname' # your registered konzum klik first and last name
gmail_user = 'email@example.com' # sending email account
gmail_password = 'somepassword' # sending email account password
...
```

**requirements:**

* beautifulsoup4
* requests
* smtplib (should be available with python installation by default)

**run with:**

```
python Kscrape.py
```

**Note:**

This script has flaws so If you find bugs, please let me know!
